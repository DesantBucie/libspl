#include <stdio.h>

#include <spl/string.h>

int main(){
    spl_string example = spl_string_init("Hello,");
    spl_string_concat(&example, "World!", 1); // 1 adds space between `Hello` and `World`.
    printf("%s\n", example);

    spl_string_cpy(&example, "Oh Hi Mark"); // cpy removes all the text that was in string before
    printf("%s\n", example);
    
    printf("Enter something: \n");
    spl_string_get(&example);
    printf("You've entered: %s\n", example);

    spl_string_release(&example);
}
