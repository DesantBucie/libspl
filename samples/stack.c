#include <stdio.h>

#include <spl/stack.h>

int main(){
    int tab[20];
    spl_stack example = spl_stack_init(tab, 20, sizeof(int)); // stack needs table and size. 
    // If you need to use dynamic size, allocate pointer, and then pass it and size to init function. 
    
    for(int i = 0; i < 20; i++){
        spl_stack_push(&example, &i);
    }
    for(int i = 0; i < 20; i++){
        int temp = 0;
        spl_stack_pop(&example, &temp);
        printf("%d ", temp);
    }
    printf("\n");
}
