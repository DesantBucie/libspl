#include <stdio.h>

#include <spl/vector.h>

int main(){
    spl_vector *example = spl_vector_init(NULL, sizeof(double));
    double *ptr = example->vector;
    for(int i = 0; i < 20; i++){
        double temp =  i;
        spl_vector_pushback(example, &temp);
        ptr = example->vector; // Unfortunately, every resize vector reallocates so we need to reassign after every push to be sure.
        printf("Vector size: %zu\n", example->_size);
    }
    puts("VALUES");
    for(int i = 0; i < 20; i++) {
        printf("%f\n", ptr[i]);
    }
    spl_vector_release(example);
}
