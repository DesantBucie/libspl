#include <stdio.h>

#include <spl/list.h> // in my case dylib is in /usr/local/lib and header files are in /usr/local/include

int main(){
    spl_list *example = spl_list_init(NULL, sizeof(int));

    for(int i = 0; i < 20; i++) {
        spl_list_push_back(example, &i);
    }
    spl_node *temp = (spl_node *) example->head; 

    spl_list_print(int, d, temp); // This is macro. So don't overuse it too much, cause it will be slower than function you can make yourself.
    // int is a type of list data 
    // d is for printf("%d"...)
    // temp is node that we created earlier.
    // The float would be: spl_list_print(float, f, temp);
    
    spl_list_release(example);
}
