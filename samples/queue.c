#include <stdio.h>

#include <spl/queue.h>

int main(){
    spl_queue *example = spl_queue_init(NULL, 30, sizeof(int));
    
    for(int i = 0; i < 20; i++){
        spl_enqueue(example, &i); // Unfortunately you cannot just pass constant. It has to be a variable address.
    }

    spl_queue_release(example);
}


