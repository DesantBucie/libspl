# Small Portable Library

This is an implementation of the structures I learned about in uni. I will push here everything I think will be useful for me in the future and call it a library.

## THIS IS NOT YET FINISHED

## Features

* generic lists
* generic vectors
* strings
* generic stack structures
* generic queue

## Usage

1. Compile using make(BSD, or GNU): `make`

1. Copy .so/.dylib to lib folder, for example `/usr/local/lib/`

1. Make folder in include, for example `/usr/local/include/spl` and copy all .h files there.

Compile your program: `clang yourprogram.c -lspl`

or statically link,(better for end user).

## "API" usage

Check manuals: `man ./man/list.3`, or copy man folder content to your system one.

