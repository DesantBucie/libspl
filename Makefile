
OBJ=stack.o list.o string.o queue.o vector.o

mpb: $(OBJ)
	@if [ $$(uname -s) == "Darwin" ]; then \
		export LIBEXT="dylib"; \
	else \
		export LIBEXT="so"; \
	fi; \
	cc -shared $(OBJ) -Os -o libspl.$$LIBEXT
clean:
	rm -f *.o libspl*
install: mpb
	if [ -x $$(command -v doas) ]; then \
		export DOAS=doas; \
	else \
		export DOAS=sudo; \
	fi; \
	$$DOAS rm -f /usr/local/lib/libspl.*; \
	$$DOAS cp libspl.* /usr/local/lib/; \
	$$DOAS cp src/*.h /usr/local/include/spl/;
stack.o: src/stack.c
list.o: src/list.c
string.o: src/string.c
queue.o: src/queue.c
vector.o: src/vector.c

%.o: src/%.c
	clang -c -O2 -o $<
