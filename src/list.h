#ifndef _SPL_LIST_H_
#define _SPL_LIST_H_

#include <stdlib.h>
#include <string.h>

#define spl_list_print(type, printf_sign, node) \
        while(node != NULL) { \
            type *ptr = (type *) node->data; \
            printf("%"#printf_sign" ", *ptr); \
            node = node->next; \
        } \
        printf("\n");

typedef struct spl_node spl_node;

struct spl_node {
    void *data;
    spl_node *next;
};

typedef struct {
    const spl_node *const head;
    const spl_node *const tail;
    const size_t _data_size;
} spl_list;

spl_list *spl_list_init(spl_list *l, size_t data_size);

static spl_node *create_node(void *value, size_t data_size);

int spl_list_push_pos(spl_list *list, void *value, size_t position),
    spl_list_copy_pos(spl_list *list, void *ref, size_t position),
    spl_list_remove_pos(spl_list *list, void *ref, size_t position),

    spl_list_push_back(spl_list *l, void *value),
    spl_list_push_front(spl_list *l, void *value),
    spl_list_remove_front(spl_list *l, void *ref),
    spl_list_remove_back(spl_list *l, void *ref),

    spl_list_release(spl_list *l);

#endif /*_SPL_LIST_H_ */
