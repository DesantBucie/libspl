#ifndef _SPL_STACK_H_
#define _SPL_STACK_H_

#include <string.h>
#include <stdbool.h>


typedef struct {
    void *const stack;
    const size_t size;
    const size_t data_size;
    void *const top;
} spl_stack;

spl_stack spl_stack_init(void *arr, size_t size, size_t data_size);

_Bool   spl_stack_is_full(spl_stack *stack),
        spl_stack_is_empty(spl_stack *stack);

int spl_stack_pop(spl_stack *stack, void *ref),
    spl_stack_push(spl_stack *stack, void *data);

#endif /* _SPL_STACK_H_ */
