#include "stack.h"

typedef struct stack_t stack_t;

struct stack_t {
    void *stack;
    size_t size;
    size_t data_size;
    void *top;
};

spl_stack spl_stack_init(void *arr, size_t size, size_t data_size){
    spl_stack stack = {
        .stack = arr,
        .size = size,
        .data_size = data_size,
    };
    return stack;
}
_Bool spl_stack_is_full(spl_stack *stack){
   if(stack->top < stack->stack + (stack->size) * stack->data_size){
       return 0;
   }
   else {
       return 1;
   }
}
_Bool spl_stack_is_empty(spl_stack *stack){
    if(!stack->top){
        return 1;
    }
    else{
        return 0;
    }
}
int spl_stack_pop(spl_stack *s, void *ref){
    stack_t *stack = (stack_t *)s;
    if(!stack->top){
        return -1;
    }
    if(!ref){
        return -1;
    }
    memcpy(ref, stack->top, stack->data_size);

    if(stack->top - stack->data_size < stack->stack){
        stack->top=NULL;
    }
    else{
        stack->top -= stack->data_size;
    }
    return 0;
}
int spl_stack_push(spl_stack *s, void *data){
    stack_t *stack = (stack_t *)s;

    if(!data){
        return -1;
    }
    if(!stack->top){
        stack->top = stack->stack;
    }
    else{
        stack->top += stack->data_size;
    }
    if(spl_stack_is_full(s)){
        return -1;
    }
    memcpy(stack->top, data, stack->data_size);
    return 0;
}
