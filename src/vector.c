#include "vector.h"
#include <string.h>

typedef struct vector_t vector_t;
struct vector_t {
    void *vector;
    void *tail;
    size_t _size;
    size_t _data_size;
};

static vector_t *spl_vector_resize(vector_t *vector){
    if(vector->_size == 0){
        vector->vector = malloc(vector->_data_size); 
        vector->_size = 1;
        vector->tail = vector->vector - vector->_data_size;
    }
    else {
        vector->_size *= 2;
        vector->vector = realloc(vector->vector, vector->_size * vector->_data_size);
        vector->tail = vector->vector + ( vector->_size / 2 ) * vector->_data_size - vector->_data_size; 
        // Tail is placed on the same relative position
        /// [][][][t]
        /// [][][][t][][][][] <- this can be another part of the memory 
        ///        size / 2  * data size - data size 
    }

    if(!vector->vector)
        return NULL;
    return vector;
}
spl_vector *spl_vector_init(spl_vector *v, size_t data_size){

    if(!v){
        v = malloc(sizeof(spl_vector));
        // Assuming ptr is not null it is pointer to a struct placed on a stack
    }
    if(!v)
        return NULL;
    vector_t *vector = (vector_t *) v;

    vector->_data_size = data_size;
    vector->_size = 0;
    
    spl_vector_resize(vector);
    v = (spl_vector *) vector;
    return v;
}
int spl_vector_insert(spl_vector *v, void *data, size_t position) {

    vector_t *vector = (vector_t *) v;
    if(position * vector->_data_size > (vector->tail - vector->vector))
        return -1;

    if(vector->tail + vector->_data_size >= (vector->vector + vector->_size * vector->_data_size)){
        spl_vector_resize(vector);
    }

    void *ptr = vector->vector;
    for(int i = 0; i < position; i++) {
        ptr += vector->_data_size;
    }

    void *temp = malloc(vector->_data_size);
    void *temp2 = malloc(vector->_data_size);

    if(!temp || !temp2)
        return -1;

    memcpy(temp, ptr, vector->_data_size);
    memcpy(ptr, data, vector->_data_size);
    ptr += vector->_data_size; 

    while(ptr < vector->vector + (vector->_size * vector->_data_size)){
        memcpy(temp2, ptr, vector->_data_size);
        memcpy(ptr, temp, vector->_data_size);
        memcpy(temp, temp2, vector->_data_size);
        ptr += vector->_data_size;
    }
    vector->tail += vector->_data_size;

    return 0;
}
int spl_vector_pushback(spl_vector *v, void *data){
    vector_t *vector = (vector_t *) v;

    if(vector->tail + vector->_data_size >= (vector->vector + vector->_size * vector->_data_size)){
        spl_vector_resize(vector);
    }
    vector->tail += vector->_data_size; 

    memcpy(v->tail, data, vector->_data_size);
    return 0;
}
int spl_vector_release(spl_vector *vector){
    if(vector->vector)
        free(vector->vector);
    free(vector);
    return 0;
}
