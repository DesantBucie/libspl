#ifndef _SPL_VECTOR_H_
#define _SPL_VECTOR_H_

#include <stdlib.h>
#include <string.h>

typedef struct {
    void *const vector;
    void *const tail;
    const size_t _size;
    const size_t _data_size;
}spl_vector;

spl_vector *spl_vector_init(spl_vector *vector, size_t data_size);

int spl_vector_insert(spl_vector *vector, void *data, size_t position),
    spl_vector_pushback(spl_vector *vector, void *data),
    spl_vector_release(spl_vector *vector);

#endif /* _SPL_VECTOR_H_ */
