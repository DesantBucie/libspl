#ifndef _SPL_STRING_H_
#define _SPL_STRING_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#ifndef spl_string 
    typedef char * spl_string;
#endif

int spl_string_concat(spl_string *s, char *text, _Bool space),
    spl_string_cpy(spl_string *s, char *text),
    spl_string_release(spl_string *s),
    spl_string_get(spl_string *s),
    get_string(char *ref, size_t buffer);

spl_string spl_string_init(char *text);


#endif /* _SPL_STRING_H_ */

