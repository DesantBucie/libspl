#include "string.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct string_t string_t;

struct string_t {
    spl_string str;
    size_t size;
};

int get_string(char *ref, size_t buffer){
    char c;
    int i = 0;
    while(((c = getchar()) != '\n' && c != '\0' && c != EOF)){
        ref[i] = c;
        if(i == buffer - 1) {
            ref[i+1] = '\0';
            return -1;
        }
        i++;
    }   
    ref[i] = '\0';
    return 0;
}
int spl_string_get(spl_string *s){
    if(s == NULL)
        return -1;
    string_t *string = (string_t *)s;
    size_t const size = 4096;
    string->str = realloc(string->str, size); // Realloc should ensure, that if string was allocated it will be freed, if not it will allocated anyways
    char c;
    int i = 0;
    while((c = getchar()) != '\n' && c != '\0' && c != EOF) {
        string->str[i] = c;
        if(i == size - 1) {
            string->str[i+1] = '\0';
            printf("Warning: Buffer limit exceeded, some content may be cutted");
            return -1;
        }
        i++;
    }   
    string->str[i] = '\0';
    return 0;
}
int spl_string_cpy(spl_string *s, char *text){
    string_t *string = (string_t *)s; // Because it is struct, and char * is first member, it's also struct pointer.
    if(text == NULL) {
        return -1;
    }
    size_t size = strlen(text);
    string->size = size + 1;
    string->str = malloc(string->size);

    if(string->str == NULL) {
        return -1;
    }
    strlcpy(string->str, text, string->size);
    return 0;
}
int spl_string_concat(spl_string *s, char *text, _Bool space) {

    string_t *string = (string_t *)s;

    if(!text && !space){
        return -1;
    }

    size_t size = space ? strlen(text) + 1 : strlen(text);
    string->size += size;
    string->str = realloc(string->str, string->size);
    if(!string->str)
        return -1;
    if(space){
        char *sp = " ";
        strlcat(string->str, sp, string->size);
    }
    strlcat(string->str, text, string->size);
    return 0;
}
spl_string spl_string_init(char *text){

    string_t *s = malloc(sizeof(string_t));
    if(s == NULL){
        return NULL;
    }
    if(text) {
        spl_string_cpy(&s->str, text);
    }
    return s->str;
}

int spl_string_release(spl_string *s){

    string_t *string = (string_t *)s;

    free(string->str);

    free(string);

    return 0;
}
