#ifndef _SPL_BINARY_TREE_
#define _SPL_BINARY_TREE_

#include <stdlib.h>

typedef struct spl_tree spl_tree;
typedef struct spl_tree_node spl_tree_node;

struct spl_tree_node {
    void *data;
    spl_tree_node *left;
    spl_tree_node *right;
};
struct spl_tree {
    struct spl_tree_node *top;
    size_t const data_size;
};
#endif
