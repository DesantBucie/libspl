#ifndef _SPL_QUEUE_H_
#define _SPL_QUEUE_H_

#include <stdlib.h>
#include <string.h>

typedef struct {
    void *const q;
    void *const head; 
    void *const tail;
    const size_t _size;
    const size_t _type_size;
} spl_queue;

enum SPL_ERRORS{OK, PASSED_NULL_PTR, MALLOC_FAIL, REALLOC_FAIL, STRUCTURE_EMPTY};

spl_queue *spl_queue_init(spl_queue *q, size_t size, size_t type_size);

int spl_enqueue(spl_queue *q, void *input),
    spl_dequeue(spl_queue *q, void *ref),  
    spl_queue_release(spl_queue *q),
    spl_queue_resize(spl_queue *q, size_t size);

#endif /* _SPL_QUEUE_H_ */
