#include "queue.h"

typedef struct queue queue_t;
struct queue {
    void *q;
    void *head; 
    void *tail;
    size_t _size;
    size_t _type_size;
};
int spl_enqueue(spl_queue *queue, void *input) {

    queue_t *q = (queue_t*)queue;
    if(q->tail == NULL) {
        q->tail = q->q;
    }
    else {
        if (q->tail + q->_type_size == q->head){
            return 2;
        }
        if(q->tail + q->_type_size <= q->q + q->_size){
            q->tail += q->_type_size;   // I expect pointer to move 1b, as is void *, so this is needed.
        }                               // UPDATE: Indeed it is moving 1b
        else{
            q ->tail = q->q;
        }
    }
   
    if(q->head == NULL){
        q->head = q->tail;
    }
    memcpy(q->tail, input, q->_type_size); 

    return 0;
}
int spl_dequeue(spl_queue *queue, void *ref){

    queue_t *q = (queue_t*)queue;
    if(q->head == NULL)
        return PASSED_NULL_PTR;

    memcpy(ref, q->head, q->_type_size);

    if(q->head == q->tail){
        q->head = NULL;
        return STRUCTURE_EMPTY;
    }
    if(q->head + q->_type_size == q->head){
        return 2;
    }
    if(q->head + q->_type_size <= q->q + q->_size){
        q->head += q->_type_size;
    }
    else{
        q->head = q->q;
    }
    return OK;
}
int spl_queue_release(spl_queue *q){
    if(q->q != NULL) {
        free(q->q);
    }
    free(q);
    return 0;
}
int spl_queue_resize(spl_queue *queue, size_t size){

    queue_t *q = (queue_t*)queue;
    if(q->q == NULL) {
        return 1;
    }
    if(size > q->_size){
        q->q = realloc(q->q, size);    
    }
    else {
        return 2;
    }
    if(q->q == NULL){
        return 1;
    }
    return OK;
}
spl_queue *spl_queue_init(spl_queue *queue, size_t size, size_t type_size){

    queue_t *q = (queue_t*)queue;

    if(q == NULL){
        q = malloc(sizeof(spl_queue));
    }
    if(q == NULL){
        return NULL;
    }        
    if(size < 2) {
        q->_size = 10 * type_size;
    }
    else {
        q->_size = size * type_size;
    }
    q->q = malloc(size);
    if(q->q == NULL){
        return NULL;
    }
    q->head = NULL;
    q->tail = NULL;

    q->_type_size = type_size;

    return queue;  
}

