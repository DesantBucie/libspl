#include "list.h"

typedef struct list_t list_t;

struct list_t {
    struct spl_node *head;
    struct spl_node *tail;
    size_t _data_size;
};

static struct spl_node *create_node(void *value, size_t data_size) {

    struct spl_node *result = (struct spl_node *)malloc(sizeof(struct spl_node));
    if(!result) {
        return NULL;
    }
    result->data = malloc(data_size);
    if(!result->data) {
        return NULL;
    }
    memcpy(result->data, value, data_size);
    result->next = NULL;
    return result;
}
spl_list *spl_list_init(spl_list *list, size_t data_size) {

    list_t *l = (list_t *)list;
    l = malloc(sizeof(list_t));

    if(!l) {
        return NULL;
    }
    l->head = NULL;
    l->tail = NULL;
    l->_data_size = data_size;
    list = (spl_list *)l;

    return list;
}
int spl_list_release(spl_list *list) {
    list_t *l = (list_t *) list;

    spl_node *temp = l->head;
    spl_node *temp2;
    while(temp != NULL) {
        temp2 = temp;
        temp = temp->next;
        free(temp2);
    }
    return 0;
}
int spl_list_copy_pos(spl_list *list, void *ref, size_t position) {
    list_t *l= (list_t*) list;
    struct spl_node *temp = l->head;
    int i = 0;
    while(i < position - 1) {
        if(!temp->next) {
            return -1;
        }
        temp = temp->next;
        i++;
    }
    if(ref) {
        memcpy(ref, temp->next->data, l->_data_size);
    }
    return 0;
}
int spl_list_push_pos(spl_list *list, void *value, size_t position) {
    if(position == 0) {
        return spl_list_push_front(list, value);
    }
    list_t *l = (list_t*) list;
    struct spl_node *temp = l->head;
    struct spl_node *result = create_node(value, l->_data_size);
    int i = 0;
    while(i < position - 1) {
        if(temp->next == NULL) {
            return -1;
        }
        temp = temp->next; 
    }
    if(temp->next == NULL) {
        temp->next = result;
        l->tail = result;
    }
    else {
        result->next = temp->next;
        temp->next = result;
    }
    return 0;
}
int spl_list_push_back(spl_list *list, void *value) {

    list_t *l = (list_t*) list;
    struct spl_node *result = create_node(value, l->_data_size);

    if(l->head == NULL) {
        l->head = result;
    }
    if(l->tail != NULL) { 
        l->tail->next = result;
    }
    l->tail = result;
    return 0;
}
int spl_list_push_front(spl_list *list, void *value) {
    list_t *l = (list_t*) list;
    struct spl_node *result = create_node(value, l->_data_size);

    result->next = l->head;
    l->head = result;

    return 0;
}
int spl_list_remove_front(spl_list *list, void *ref) {
    list_t *l = (list_t *)list;
    struct spl_node *temp = l->head;

    l->head = l->head->next;

    if(ref) {
        memcpy(ref, temp->data, l->_data_size);
    }
    free(temp->data);
    free(temp);

    return 0;
}
int spl_list_remove_pos(spl_list *list, void *ref, size_t position) {
    if(position == 0) {
        return spl_list_remove_front(list, ref);
    }
    list_t *l= (list_t*) list;
    struct spl_node *temp = l->head;
    int i = 0;
    while(i < position - 1) {
        if(!temp->next) {
            return -1;
        }
        temp=temp->next;
        i++;
    }
    struct spl_node *temp2 = temp;
    if(temp->next->next != NULL) {
        temp->next = temp->next->next; 
    }
    else {
        temp->next = NULL;
    }
    if(ref) {
        memcpy(ref, temp2->next->data, l->_data_size);
    }
    free(temp2->next->data);
    free(temp2->next);

    return 0;
}
int spl_list_remove_back(spl_list *list, void *ref) {
    list_t *l= (list_t*) list;
    struct spl_node *temp = l->head;
    while(temp->next->next != NULL) {
        temp=temp->next;
    }
    l->tail = temp;

    if(ref) {
        memcpy(ref, l->tail->next->data, l->_data_size);
    }
    free(l->tail->next->data);
    free(l->tail->next);

    l->tail->next = NULL;
    return 0;
}
