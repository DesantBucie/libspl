#include "binary_tree.h"
#include <string.h>

typedef struct spl_tree_node node_t;

static node_t *create_node(spl_tree *t){
    node_t *node = malloc(sizeof(node_t));
    if(node == NULL){
        return NULL;
    }
    node->data = malloc(t->data_size);
    if(node->data == NULL) {
        return NULL;
    } 
    node->left = NULL;
    node->right = NULL;

    return node;
}
spl_tree *spl_tree_init(spl_tree *t, size_t data_size) {
    struct spl_tree *tree = (struct spl_tree *) t;

    if(tree == NULL){
        tree = malloc(sizeof(spl_tree));
    }
    if(tree == NULL){
        return NULL;
    }
    tree->top = NULL;
    return t;
}
int spl_tree_push_value(spl_tree *t, void *data) {
    if(data == NULL || t == NULL){
        return -1;
    }
    struct spl_tree *tree = (struct spl_tree *) t;
    
    node_t *node = create_node(t);
    
    memcpy(node->data, data, t->data_size);

    if(tree->top == NULL){
        tree->top = node;
    }
    else {
         
    }
    
    return 0;
}
static int spl_tree_release_node(node_t *node){
    if(node == NULL){
        return -1;
    }

    spl_tree_release_node(node->left);
    spl_tree_release_node(node->right);
    free(node);

    return 0;
}
int spl_tree_release(spl_tree *t){
    spl_tree_release_node(t->top); 
    free(t);
    return 0;
}
